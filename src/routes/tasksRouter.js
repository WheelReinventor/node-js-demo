const TasksService = require('../services/tasksService');

function setupRoutes(app, connection) {
    const tasksService = new TasksService(connection);

    app.get('/tasks', async (req, res) => {
        try {
            const tasks = await tasksService.getTasks();
            res.json(tasks);
        } catch (error) {
            console.log('An error occurred while retrieving tasks:', error);
            res.status(500).send('An error occurred while retrieving tasks');
        }
    });

    app.get('/tasks/:id', async (req, res) => {
        const taskId = req.params.id;
        try {
            const task = await tasksService.getTaskById(taskId);
            if (task) {
                res.json(task);
            } else {
                res.status(404).send('Task not found');
            }
        } catch (error) {
            console.log('An error occurred while retrieving the task:', error);
            res.status(500).send('An error occurred while retrieving the task');
        }
    });

    app.post('/tasks', async (req, res) => {
        const { title, description } = req.body;
        try {
            await tasksService.createTask(title, description);
        } catch (error) {
            console.log('An error occurred while creating the task:', error);
            res.status(500).send('An error occurred while creating the task');
        }
    });

    app.put('/tasks/:id', async (req, res) => {
        const taskId = req.params.id;
        const { title, description } = req.body;
        try {
            const updatedTask = await tasksService.updateTask(taskId, title, description);
            if (updatedTask) {
                res.json(updatedTask);
            } else {
                res.status(404).send('Task not found');
            }
        } catch (error) {
            console.log('An error occurred while updating the task:', error);
            res.status(500).send('An error occurred while updating the task');
        }
    });

    app.delete('/tasks/:id', async (req, res) => {
        const taskId = req.params.id;
        try {
            const deletedTaskId = await tasksService.deleteTaskById(taskId);
            if (deletedTaskId) {
                res.send(`Task with ID ${deletedTaskId} deleted successfully`);
            } else {
                res.status(404).send('Task not found');
            }
        } catch (error) {
            console.log('An error occurred while deleting the task:', error);
            res.status(500).send('An error occurred while deleting the task');
        }
    });
}

module.exports = setupRoutes;