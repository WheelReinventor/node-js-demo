const mysql = require('mysql');

function createConnection() {
    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: '', // no password
        database: 'node_js_demo',
    });
    return connection;
}

module.exports = {
    createConnection
};