class TasksService {
    constructor(connection) {
        this.connection = connection;
    }

    async getTasks() {
        return new Promise((resolve, reject) => {
            const query = 'SELECT * FROM tasks';
            this.connection.query(query, (error, results) => {
                if (error) {
                    reject(new Error('Failed to retrieve tasks'));
                } else {
                    resolve(results);
                }
            });
        });
    }

    async getTaskById(taskId) {
        return new Promise((resolve, reject) => {
            const query = 'SELECT * FROM tasks WHERE id = ?';
            this.connection.query(query, [taskId], (error, results) => {
                if (error) {
                    reject(new Error('Failed to retrieve the task'));
                } else {
                    if (results.length > 0) {
                        resolve(results[0]);
                    } else {
                        resolve(null)
                    }
                }
            });
        });
    }

    async createTask(title, description) {
        return new Promise((resolve, reject) => {
            const createdAt = new Date();
            const updatedAt = new Date();
            const query = 'INSERT INTO tasks (title, description, created_at, updated_at) VALUES (?, ?, ?, ?)';
            const values = [title, description, createdAt, updatedAt];
            this.connection.query(query, values, (error, result) => {
                if (error) {
                    reject(new Error('Failed to create task'));
                } else {
                    resolve(result.insertId);
                }
            });
        });
    }

    async updateTask(taskId, title, description) {
        return new Promise((resolve, reject) => {
            const updatedAt = new Date();
            const query = 'UPDATE tasks SET title = ?, description = ?, updated_at = ? WHERE id = ?';
            const values = [title, description, updatedAt, taskId];
            this.connection.query(query, values, (error, result) => {
                if (error) {
                    reject(new Error('Failed to update the task'));
                } else {
                    if (result.affectedRows > 0) {
                        resolve({ taskId, title, description, updatedAt });
                    } else {
                        resolve(null);
                    }
                }
            });
        });
    }

    async deleteTaskById(taskId) {
        return new Promise((resolve, reject) => {
            const query = 'DELETE FROM tasks WHERE id = ?';
            this.connection.query(query, [taskId], (error, result) => {
                if (error) {
                    reject(new Error('Failed to delete the task'));
                } else {
                    if (result.affectedRows > 0) {
                        resolve(taskId);
                    } else {
                        resolve(null);
                    }
                }
            });
        });
    }
}

module.exports = TasksService;