const { createConnection } = require('./db');
const express = require('express');
const app = express();
const port = 3000;

const tasksRouter = require('./routes/tasksRouter')

// Middleware to parse JSON bodies
app.use(express.json());

// Connect to the MySQL database
const connection = createConnection();
connection.connect((err) => {
    if (err) {
        console.error('Error connecting to MySQL database:', err);
        return;
    }
    console.log('Connected to MySQL database!');
});

// Setup routes
tasksRouter(app, connection);

// Start the server
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});